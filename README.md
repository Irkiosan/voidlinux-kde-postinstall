# kvoid

This script provides a rather complete but yet vanilla install of KDE, with a lot of usefull features to provide a functional out-of-the-box desktop experience.

### Dependencies
none, but you will need git probabely `sudo xbps-install -S git`

### How to use
Preperation:
- install Void Linux base install (glibc)
- have or create a user with root privileges
- log in to your user account

Download:

`git clone https://gitlab.com/Irkiosan/voidlinux-kde-postinstall.git`

cd into the directory:

`cd voidlinux-kde-postinstall`

Make the script executable:

`chmod +x kvoid`

Run the script:

`sudo ./kvoid`

Reboot finishes the process. From now on you will boot into SDDM/KDE Plama.

### What "kvoid" does
- process language input (currently only for SDDM keyboard layout and system spell check; Plasma language and keyboard layout has to be set manually in the system settings for now). Make sure to type lower case (e.g. **de** or **en** or **fr** or **...**)
- process username input (your username is needed for some user specific settings). The script will only allow existing users, so make sure you have set up an user beforehand (e.g. during the installation)
- process whether you have an Intel CPU (for installing microcode)
- XBPS will be updated, system updates will be installed, non-free repo will be added, and the repos will be synced
- the following tools will be installed: **curl, nano, htop, libzip, libzstd, octoxbps, wget xdg-user-dirs xdg-utils xdotool xtools**
- If Intel CPU, **microcode** will be installed and the **kernel** will be reconfigured
- If you choose Laptop, the following tools will be installed: **tlp, cpupower, upower, and powerdevil** and **tlp** will be enabled
- a KDE Plasma base will be created
  - the following packages will be installed: **xorg kde5 kde5-baseapps**
  - the following services will be enabled: **sddm, dbus, NetworkManager, acpid, and bluetoothd**. Note that enabling **elogind** is **not required** since it will be started automatically via dbus.
- the desktop experience will be enriched
  - the following packeges will be installed: **ark kdeconnect okular kcalc gwenview spectacle kcharselect partitionmanager plasma-vault plasma-disks nextcloud-client-dolphin kdewebkit plasma-firewall plasma-integration dolphin-plugins flatpak-kcm print-manager kdegraphics-thumbnailers libreoffice-kde hunspell-lang_LANG mediainfo**
- security
  - the following packages will be installed: **apparmor and ufw**
  - **apparmor** will be added to **boot parameters** in **GRUB** (GRUB will be updated)
  - **ufw** will be **enabled** and KDE-Connect and ssh will be **allowed**
- printer support
  - the following packages will be installed: **cups, cups-filters, and hplip**
  - the following services will be enabled: **cupsd and cups-browsed**
  - your **user** will be **added** to the **lpadmin group**
- codecs will be installed: **mesa-vaapi ffmpegthumbs ffmpeg ffmpegthumbnailer gst-plugins-good1 gst-plugins-ugly1 gstreamer1-pipewire libspa-ffmpeg mesa-opencl** and if Intel is chosen: **intel-video-accel mesa-intel-dri**
- sound (via Pipewire)
  - the following packages will be installed: **pipewire alsa-utils alsa-pipewire wireplumber libjack-pipewire libspa-bluetooth**
  - **alsa** service will be **enabled**
  - **pipewire, wireplumber, pipewire-pulse, and alsa** will be configured to **autostart**
- virtual machine
  - **virt-manager, virt-manager-tools, and qemu** will be installed
  - **virtlockd virtlogd virtnetworkd virtstoraged virtqemud libvirtd** services will be **enabled**
  - **libvirt** will be configured to prevent **apparmor issues**
- Flatpak
  - **flatpak** will be installed
  - **flathub** repo will be added
  - **flatseal** will be installed

After the **reboot** you will boot into SDDM/KDE Plasma
